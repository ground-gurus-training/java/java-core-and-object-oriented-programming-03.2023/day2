package ph.groungurus.day2.util;

public class Strings {
    public static boolean equals(Object object1, Object object2) {
        // guard clause
        if (object1 == null && object2 == null) {
            return true;
        }

        // guard clause
        if (object1 == null || object2 == null) {
            return false;
        }

        return object1.equals(object2);
    }
}
