package ph.groungurus.day2;

public class ArrayExample {
    public static void main(String[] args) {
        //              0    1   2
//        int stuff[] = { 11, 22, 33 };
//        System.out.println(stuff[0]);
//        System.out.println(stuff.length);

        int moreStuff[][] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        System.out.println(moreStuff[1][1]);
    }
}
