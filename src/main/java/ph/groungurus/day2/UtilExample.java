package ph.groungurus.day2;

import ph.groungurus.day2.util.Strings;

import java.util.Scanner;

public class UtilExample {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        System.out.print("Name: ");
        var name = scanner.nextLine();

        var famousNames = new String[] {
                "John",
                "Joe",
                "Smith",
                null
        };

        for (String famousName : famousNames) {
            if (Strings.equals(famousName, name)) {
                System.out.println("You have a celebrity with the same name as you!");
                break;
            }
        }

        scanner.close();
    }
}
