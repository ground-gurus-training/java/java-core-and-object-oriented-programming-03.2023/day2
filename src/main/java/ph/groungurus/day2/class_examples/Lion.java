package ph.groungurus.day2.class_examples;

public class Lion {
    public static final String FAMILY = "Feline";

    public static void hunt() {
        System.out.println("Hunting...");
    }

    public void walk() {
        System.out.println("Walking...");
    }
}
