package ph.groungurus.day2.class_examples;

public enum Day {
    MONDAY("Mon"), TUESDAY("Tue"), WEDNESDAY("Wed"), THURSDAY("Thu"),
    FRIDAY("Fri"), SATURDAY("Sat"), SUNDAY("Sun");

    final String day;

    Day(String day) {
        this.day = day;
    }

    public String getShortValue() {
        return day;
    }
}
