package ph.groungurus.day2.class_examples;

public class AnimalsMain {
    public static void main(String[] args) {
        var kuting = new Lion();
        System.out.println(kuting.FAMILY);
        kuting.hunt();

        var lion = new Lion();
        System.out.println(lion.FAMILY);
        lion.hunt();

        System.out.println(Lion.FAMILY);
        Lion.hunt();
    }
}
