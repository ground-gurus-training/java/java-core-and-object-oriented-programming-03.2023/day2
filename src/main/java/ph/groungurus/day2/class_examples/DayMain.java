package ph.groungurus.day2.class_examples;

public class DayMain {
    public static void main(String[] args) {
        var monday = Day.MONDAY;
        var tuesday = Day.TUESDAY;

        System.out.println(monday.day);
        System.out.println(tuesday.day);

        System.out.println(monday.getShortValue());
        System.out.println(tuesday.getShortValue());
    }
}
