package ph.groungurus.day2;

public class CommandLineArgumentsExample {
    public static void main(String[] args) {
        for (String arg : args) {
            System.out.println(arg);
        }
    }
}
