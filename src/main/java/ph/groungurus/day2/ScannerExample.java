package ph.groungurus.day2;

import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter your name: ");
        var name = scanner.next();
        System.out.println("Hello " + name + "!");
        scanner.close();
    }
}
